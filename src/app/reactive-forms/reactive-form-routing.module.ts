import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsComponent } from './components/reactive-forms.component';

const reactiveFormRoutes: Routes = [
  {
    path: '',
    component: ReactiveFormsComponent,
    children: [
      {
        path: '',
        children: [
          {path: 'reactiveform', loadChildren: 'app/reactive-forms/reactive-form.module#ReactiveFormModule' },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(reactiveFormRoutes)],
  exports: [RouterModule]
})
export class ReactiveFormsRoutingModule { }
