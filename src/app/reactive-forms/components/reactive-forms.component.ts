import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {User} from '../../user';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: '../html/reactive-forms.component.html',
  styleUrls: ['../styles/reactive-forms.component.scss']
})
export class ReactiveFormsComponent implements OnInit {
  userList: User[]=[];

  form: FormGroup;

  addUser(form){
    this.userList.push(this.form.value);
  }

  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z]+')]),
      contact: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(10), Validators.maxLength(10)]),
      email: new FormControl('', [Validators.required, Validators.email])
    });
  }

}
