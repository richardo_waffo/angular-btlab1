import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { ReactiveFormsRoutingModule } from './reactive-form-routing.module';
import { ReactiveFormsComponent } from './components/reactive-forms.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ReactiveFormsComponent]
})
export class ReactiveFormModule { }
