import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        children: [
          {path: 'welcom', loadChildren: 'app/full-connect/full-connect.module#FullConnectModule' },
          {path: 'gallery', loadChildren: 'app/gallery/gallery.module#GalleryModule' },
          {path: 'form01', loadChildren: 'app/form01/form01.module#Form01Module' },
          {path: 'reactiveform', loadChildren: 'app/reactive-forms/reactive-form.module#ReactiveFormModule' },
          {path: 'blog', loadChildren: 'app/blog/blog.module#BlogModule' },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
