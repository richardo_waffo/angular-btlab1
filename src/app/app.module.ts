import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule, FormGroup} from '@angular/forms';
import {HttpModule} from '@angular/http'

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './root/components/app.component';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { FormsubmitService } from './formsubmit.service';
import { FullConnectModule } from './full-connect/full-connect.module';
import { ReactiveFormModule } from './reactive-forms/reactive-form.module';
import { Form01Module } from './form01/form01.module';
import { GalleryModule } from './gallery/gallery.module';
import { BlogModule } from './blog/blog.module';
import { PokemonesService } from './blog/services/pokemones.service';
import { RealinputComponent } from './realinput/realinput.component';


@NgModule({
  declarations: [
    AppComponent,
    RealinputComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    FullConnectModule,
    ReactiveFormModule,
    Form01Module,
    GalleryModule,
    BlogModule
  ],
  providers: [FormsubmitService, PokemonesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
