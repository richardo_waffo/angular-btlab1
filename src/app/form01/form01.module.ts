import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';


import { Form01RoutingModule } from './form01-routing.module';
import { Form01Component } from './compnents/form01.component';
import { WordpressComponent } from './compnents/wordpress.component';
import { NodejsComponent } from './compnents/nodejs.component';
import { BootstrapComponent } from './compnents/bootstrap.component';
import { LearnsqlComponent } from './compnents/learnsql.component';
import { LearnjsComponent } from './compnents/learnjs.component';
import { LearnphpComponent } from './compnents/learnphp.component';
import { LearncssComponent } from './compnents/learncss.component';
import { LearnhtmlComponent } from './compnents/learnhtml.component';
import { ChatbotComponent } from './compnents/chatbot.component';

@NgModule({
  imports: [
    CommonModule,
    Form01RoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
     Form01Component,
     ChatbotComponent,
     LearnhtmlComponent,
     LearncssComponent,
     LearnphpComponent,
     LearnjsComponent,
     LearnsqlComponent, 
     BootstrapComponent, 
     NodejsComponent, 
     WordpressComponent]
})
export class Form01Module { }
