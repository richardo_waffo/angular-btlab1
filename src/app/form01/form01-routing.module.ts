import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Form01Component } from './compnents/form01.component';
import { BootstrapComponent } from './compnents/bootstrap.component';
import { ChatbotComponent } from './compnents/chatbot.component';
import { LearncssComponent } from './compnents/learncss.component';
import { LearnhtmlComponent } from './compnents/learnhtml.component';
import { LearnjsComponent } from './compnents/learnjs.component';
import { LearnphpComponent } from './compnents/learnphp.component';
import { LearnsqlComponent } from './compnents/learnsql.component';
import { NodejsComponent } from './compnents/nodejs.component';
import { WordpressComponent } from './compnents/wordpress.component';

const form01Routes: Routes = [
  {
    path: '',
    component: Form01Component,
    children: [
      {
        path: '',
        children: [
          {path: 'bootstrap', component: BootstrapComponent},
          {path: 'chatbot', component: ChatbotComponent},
          {path: 'learncss', component: LearncssComponent},
          {path: 'learnhtml', component: LearnhtmlComponent},
          {path: 'learnjs', component: LearnjsComponent},
          {path: 'learnphp', component: LearnphpComponent},
          {path: 'learnsql', component: LearnsqlComponent},
          {path: 'nodejs', component: NodejsComponent},
          {path: 'wordpress', component: WordpressComponent},
          {path: '*', redirectTo: "chatbot"},
          {path: 'form01', loadChildren: 'app/form01/form01.module#Form01Module' },
         
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(form01Routes)],
  exports: [RouterModule]
})
export class Form01RoutingModule { }
