import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form01',
  templateUrl: '../html/form01.component.html',
  styleUrls: ['../styles/form01.component.scss']
  
})
export class Form01Component implements OnInit {

  constructor() { }

  public employeeRecord: any = [
    {eName:'Pierre', eCity:'Douala', eEmail:'pierre@p.com'},
    {eName:'Jean', eCity:'Yaounde', eEmail:'jean@j.com'},
    {eName:'Paul', eCity:'Limbe', eEmail:'paul@p.com'},
    {eName:'Isac', eCity:'Buea', eEmail:'isac@i.com'}
  ]
  
  ngOnInit() {
  }

  public selectedEmployeeData: any = {
    selectedName: '',
    selectedCity: '',
    selectedEmail: ''
  };
  public getRecord(data: any):void {
    this.selectedEmployeeData = data;
  }
}
