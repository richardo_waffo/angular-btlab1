import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';

@Component({
  selector: 'app-nodejs',
  templateUrl: '../html/nodejs.component.html',
  styleUrls: ['../styles/nodejs.component.scss'],
  
})
export class NodejsComponent implements OnInit {

  @Input('name') employeeName: string;
  @Input('city') employeeCity: string;
  @Input('email') employeeEmail: string;
  @Output() sendRecord: EventEmitter<any> = new EventEmitter();

  form: FormGroup;

  constructor() { }

  public emitSendRecordEvent(){
    let selectedEmployeeOb: any = {
      selectedName: this.employeeName,
      selectedCity: this.employeeCity,
      selectedEmail: this.employeeEmail
    }
    this.sendRecord.emit(selectedEmployeeOb);
  }

  ngOnInit() {
    
  }

}
