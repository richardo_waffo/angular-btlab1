import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-learncss',
  templateUrl: '../html/learncss.component.html',
  styleUrls: ['../styles/learncss.component.scss']
})
export class LearncssComponent implements OnInit {

  @Output() valueChange = new EventEmitter();
    counter = 0;
    valueChanged() {
        this.counter = this.counter + 1;
        this.valueChange.emit(this.counter);
    }

  constructor() { }

  ngOnInit() {
  }

}
