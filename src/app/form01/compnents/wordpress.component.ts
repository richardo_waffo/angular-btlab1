import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wordpress',
  templateUrl: '../html/wordpress.component.html',
  styleUrls: ['../styles/wordpress.component.scss']
})
export class WordpressComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
