import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bootstrap',
  templateUrl: '../html/bootstrap.component.html',
  styleUrls: ['../styles/bootstrap.component.scss']
})
export class BootstrapComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
