import { Component, ViewChild, Injectable, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { FormsubmitService } from '../../formsubmit.service';

@Component({
  selector: 'app-gallery',
  templateUrl: '../html/gallery.component.html',
  styleUrls: ['../styles/gallery.component.scss']
})
@Injectable()
export class GalleryComponent implements OnInit {

  UserForm: FormGroup;
  UserImageFile: File;
  @ViewChild('UserImage') User_Image;

  constructor(private fb: FormBuilder, private fbservice: FormsubmitService) { 
    this.UserForm = this.fb.group({
        'Email': ['', Validators.required],
        'Password': ['', Validators.required],
        'UserImage': ['', Validators.required],
      });
      
  }

  OnSubmit(value){
   const Image = this.User_Image.nativeElement;
   if(Image.files && Image.files[0]){
     this.UserImageFile = Image.files[0];
   }

   const ImageFile: File = this.UserImageFile;
   console.log(ImageFile);

   const formData: FormData = new FormData();
   formData.append(name, 'Email', value.Email);
   formData.append(name, 'Password', value.Password);
   formData.append(name, 'UserImage', ImageFile.name);

   this.fbservice.submitData(formData).subscribe(
      data => {
       console.log(data);
     }
   );
  }

  ngOnInit() {
  }

}
