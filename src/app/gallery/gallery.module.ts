import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule, FormGroup} from '@angular/forms';
import {HttpModule} from '@angular/http'


import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './components/gallery.component';
import { FormsubmitService } from '../formsubmit.service';

@NgModule({
  imports: [
    CommonModule,
    GalleryRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [FormsubmitService],
  declarations: [GalleryComponent]
})
export class GalleryModule { }
