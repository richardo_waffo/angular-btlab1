import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GalleryComponent } from './components/gallery.component';

const routes: Routes = [
  {
    path: '',
    component: GalleryComponent,
    children: [
      {
        path: '',
        children: [
          {path: 'gallery', loadChildren: 'app/gallery/gallery.module#GalleryModule' },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GalleryRoutingModule { }
