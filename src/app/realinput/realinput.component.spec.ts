import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealinputComponent } from './realinput.component';

describe('RealinputComponent', () => {
  let component: RealinputComponent;
  let fixture: ComponentFixture<RealinputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealinputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealinputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
