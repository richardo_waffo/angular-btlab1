import { Injectable } from '@angular/core';

export interface PokemonRules{
  id: number;
  nombre: string;
  imagen: string;
  tipo: string;
  bio: string;
}

@Injectable()
export class PokemonesService {

  pokemones:PokemonRules[] = [
    {
        "id":0,
        "nombre": "autre-type",
        "imagen":"19.jpg",
        "tipo": "planto",
        "bio": "We will understand what is going on and Shell solution works under the hood."
    },
    {
        "id":1,
        "nombre": "plat-champignon",
        "imagen":"9.jpg",
        "tipo": "Feugo",
        "bio": "Understand what is going on and Shell solution works under the hood."
    },
    {
        "id":2,
        "nombre": "Pleurote",
        "imagen":"17.jpg",
        "tipo": "Agua",
        "bio": "We what is going on and Shell solution works under the hood."
    },
    {
        "id":3,
        "nombre": "Pleurotus",
        "imagen":"8.jpg",
        "tipo": "Electico",
        "bio": "Going on and Shell solution works under the hood."
    },
    {
        "id":4,
        "nombre": "rotie-champignon",
        "imagen":"13.jpg",
        "tipo": "Electico",
        "bio": "Going on and Shell solution works under the hood."
    }
];

obtenerPokemones(){
  return this.pokemones;
}

obtenerPokemon(id:number){
  let pokeArr = [];

  for(let pokemon of this.pokemones){
      if(pokemon.id == id){
          pokeArr.push(pokemon);
       }
   }

   return pokeArr; 
}

buscarPokemon(termino:string){
    let pokeArr = [];
   termino = termino.toLowerCase();
   for(let pokemon of this.pokemones){
       let nombre = pokemon.nombre.toLowerCase();
       if(nombre.indexOf(termino) >= 0){
           pokeArr.push(pokemon);
       }
   }
   return pokeArr;
}

  constructor() { }

}
