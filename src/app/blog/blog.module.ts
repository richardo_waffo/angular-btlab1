import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule, FormGroup} from '@angular/forms';
import {HttpModule} from '@angular/http'

import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './components/blog.component';
import { PokemonesService } from './services/pokemones.service';
import { BuscadorPokemonComponent } from './components/buscador-pokemon.component';
import { PokemonInfoComponent } from './components/pokemon-info.component';
import { PokemonesComponent } from './components/pokemones.component';
import { HomeComponent } from './components/home.component';
import { HeaderComponent } from './components/header.component';

@NgModule({
  imports: [
    CommonModule,
    BlogRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [PokemonesService],
  declarations: [
    BlogComponent,
    HeaderComponent,
    HomeComponent,
    PokemonesComponent,
    PokemonInfoComponent,
    BuscadorPokemonComponent
  ]
})
export class BlogModule { }
