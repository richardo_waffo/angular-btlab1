import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: '../html/blog.component.html',
  styleUrls: ['../styles/blog.component.scss']
})
export class BlogComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
