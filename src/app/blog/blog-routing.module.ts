import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogComponent } from './components/blog.component';
import { BuscadorPokemonComponent } from './components/buscador-pokemon.component';
import { PokemonInfoComponent } from './components/pokemon-info.component';
import { PokemonesComponent } from './components/pokemones.component';
import { HomeComponent } from './components/home.component';

const blogRoutes: Routes = [
  {
    path: '',
    component: BlogComponent,
    children: [
      {
        path: '',
        children: [
          {path: 'home', component: HomeComponent},
          {path: 'pokemones', component: PokemonesComponent},
          {path: 'pokemon/:id', component: PokemonInfoComponent},
          {path: 'buscar/:numbre', component: BuscadorPokemonComponent},
          {path: '**', redirectTo: "home"},
          {path: 'blog', loadChildren: 'app/blog/blog.module#BlogModule' },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(blogRoutes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
