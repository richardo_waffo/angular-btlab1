import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FullConnectComponent} from './components/full-connect.component'
import { VotesComponent } from './components/votes.component';
import { ArticleComponent } from './components/article.component';

const fullConnectRoutes: Routes = [
  {
    path: '',
    component: FullConnectComponent,
    children: [
      {
        path: '',
        children: [
          {path: 'votes', component: VotesComponent},
          {path: 'add', component: ArticleComponent},
          {path: 'welcom', loadChildren: 'app/full-connect/full-connect.module#FullConnectModule' },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(fullConnectRoutes)],
  exports: [RouterModule]
})
export class FullConnectRoutingModule { }
