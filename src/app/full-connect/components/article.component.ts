import { Component, OnInit } from '@angular/core';

import { Article } from '../article';

@Component({
  selector: 'article',
  host: {
    class: 'row'
  },
  templateUrl: '../html/article.component.html',
  styleUrls: ['../styles/article.component.scss'],
  inputs: ['article']
})
export class ArticleComponent implements OnInit {

  article: Article;

  constructor() { }

  voteUp():boolean {
    this.article.voteUp();  
    return false;
  }

  voteDown():boolean {
    this.article.voteDown();    
    return false;
  }

  ngOnInit() {
  }

}
