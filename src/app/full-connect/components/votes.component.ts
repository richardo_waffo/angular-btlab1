import { Component, OnInit } from '@angular/core';

import { Article } from '../article';

import { ArticleComponent } from './article.component';

@Component({
  selector: 'app-votes',
  templateUrl: '../html/votes.component.html',
  styleUrls: ['../styles/votes.component.scss']
})
export class VotesComponent implements OnInit {

  title:string = "Votes app";
  articles: Article[];

  constructor() { 
    this.articles = [
      new Article('Angular 2', 'http://angular.io', 3),
      new Article('Fazt Web', 'http://faztweb.com', 3),
      new Article('Faz Blog', 'http://blog.faztweb.com'),
  ]
  }

  addArticle(
    title: HTMLInputElement,
    link:HTMLInputElement
  ):boolean {
      console.log(`Article Added ${title.value}, link: ${link.value}`);
      this.articles.push(
          new Article(title.value, link.value)
      );
      title.value = '';
      link.value = '';
      return false;
    }

  sortedArticles(): Article[] {
    return this.articles.sort((a: Article, b:Article) => b.votes - a.votes);
  }

  ngOnInit() {
  }

}
