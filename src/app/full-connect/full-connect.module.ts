import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FullConnectRoutingModule } from './full-connect-routing.module';
import { FullConnectComponent } from './components/full-connect.component';
import {GalleryModule} from '../gallery/gallery.module';
import {Form01Module} from '../form01/form01.module'
import { ReactiveFormModule } from '../reactive-forms/reactive-form.module';
import { VotesComponent } from './components/votes.component';
import { ArticleComponent } from './components/article.component';
import { BlogModule } from '../blog/blog.module';

@NgModule({
  imports: [
    CommonModule,
    FullConnectRoutingModule,
    GalleryModule,
    Form01Module,
    ReactiveFormModule,
    BlogModule
  ],
 
declarations: [FullConnectComponent, VotesComponent, ArticleComponent]
})
export class FullConnectModule { }
